"""Neural network class

This module implements basic functions in order to compute CNN.
"""
import torch
import torch.nn as nn
import torchvision


def model_cnn(model_str):
    if model_str == 'vgg11':
        model_out_cnn = torchvision.models.vgg11
    elif model_str == 'vgg13':
        model_out_cnn = torchvision.models.vgg13
    elif model_str == 'vgg16':
        model_out_cnn = torchvision.models.vgg16
    elif model_str == 'vgg19':
        model_out_cnn = torchvision.models.vgg19
    else:
        raise NameError('Not a valid neural network name.')
    return model_out_cnn


def differentiable(net_feat):
    """
    Inputs are features from a CNN. These can obtained by using
    input = torchvision.models.vgg19(True).features
    This function replaces non differentiable non-linear functions
    in the network by differentiable ones.
    """
    list_feat = list(net_feat)
    list_feat_new = []
    for name_mod in list_feat:
        # replacing ReLU
        if name_mod.__class__.__name__ is 'ReLU':
            name_mod = nn.CELU(inplace=True)
        # replacing maxpooling
        if name_mod.__class__.__name__ is 'MaxPool2d':
            name_mod = nn.AvgPool2d(2, stride=2, padding=0, ceil_mode=False)
        list_feat_new.append(name_mod)
    net_feat_new = nn.Sequential(*list_feat_new)
    return net_feat_new


def rescaling(net_feat):  # normally features of VGG are between 0 and 255
    list_feat = list(net_feat)
    list_feat_new = []
    alpha = 255
    for name_mod in list_feat:
        if name_mod.__class__.__name__ is 'Conv2d':
            name_mod.bias = nn.Parameter(name_mod.bias / alpha)
        list_feat_new.append(name_mod)
    net_feat_new = nn.Sequential(*list_feat_new)
    return net_feat_new


class CNN(nn.Module):

    def __init__(self, params_CNN):
        super(CNN, self).__init__()
        self.layers = params_CNN['layers']  # tuple
        self.nb_layer = len(params_CNN['layers'])
        model_out = model_cnn(params_CNN['model_str'])
        net = model_out(True)
        net = net.cuda()
        net_feat = net.features
        net_feat = differentiable(net_feat)
        net_feat = rescaling(net_feat)
        self.model = net_feat
        for param in self.model.parameters():
            param.requires_grad = False

    def forward(self, x):
        L = self.nb_layer
        features = [0 for k in range(L)]
        l = 0
        for name, layer in enumerate(self.model):
            x = layer(x)
            if name in self.layers:                
                features[l] = x
                l += 1
            if l == L:
                break
        return features
