from torchvision import transforms
import torch


def preprocessing_vgg(img):
    img_vgg = transforms.ToTensor()(img)
    img_vgg = img_vgg[torch.LongTensor([2, 1, 0])]
    img_vgg = transforms.Normalize(
        mean=[0.40760392, 0.45795686, 0.48501961], std=[1, 1, 1])(img_vgg)
    return img_vgg


def postprocessing_vgg(img_vgg):
    img = img_vgg
    img = transforms.Normalize(mean=[-0.40760392, -0.45795686, -0.48501961],
                               std=[1, 1, 1])(img)
    img = img[torch.LongTensor([2, 1, 0])]
    return img


def back2img(params, x):
    x_torch = x.detach().cpu()
    for k in range(x_torch.shape[0]):
        x_torch_k = postprocessing_vgg(x_torch[k])
        x_torch[k] = x_torch_k
    return x_torch


def back2img_x0(params, x):
    x_torch = x.detach().cpu()
    for k in range(x_torch.shape[0]):
        x_torch_k = torch.stack([postprocessing_vgg(x_torch[k].detach().cpu())])
        x_torch[k] = x_torch_k
    return x_torch
