import numpy as np
import torch


def torch_extension(fun):
    """Define torch extension for numpy functions."""
    def wrapper(*args, **kwargs):
        list_arg = list(args)
        n_arg_torch = 0
        for n in range(len(list_arg)):
            arg = list_arg[n]
            if type(arg).__module__ is 'torch':
                arg_np = arg.cpu().detach().numpy()
                n_arg_torch += 1
            else:
                arg_np = arg
            list_arg[n] = arg_np

        res_np = fun(*list_arg, **kwargs)

        res = torch.from_numpy(np.array(res_np)) if (
            res_np is not None) & (n_arg_torch > 0) else res_np

        return res

    return wrapper


@torch_extension
def imshow(x, title, frameon=False):
    import matplotlib.pyplot as plt
    valx = x.ndim == 4
    if valx:
        x = x[0, :].transpose((1, 2, 0))
    m, n = (x.shape[0], x.shape[1])
    # clipping at 5%
    SATURATION = 5
    m = np.percentile(x, SATURATION)
    M = np.percentile(x, 100 - SATURATION)
    x = (x - m) / (M - m)
    x = x.clip(0, 1)
    plt.clf()
    if x.ndim == 2:
        plt.imshow(x, cmap='gray')
    else:
        plt.imshow(x)
    plt.title(title)
    plt.axis('off')
    plt.draw()
    plt.pause(0.01)


@torch_extension
def downsample2(x):
    from scipy.ndimage import zoom
    valx = x.ndim == 4
    if valx:
        x = x[0, :]
    x_out = zoom(x, (1, 0.5, 0.5))
    # x_out = [x[i, ::2, ::2] for i in range(x.shape[0])]
    # x_out = np.array(x_out)
    if valx:
        x_out = np.reshape(x_out, (1,) + x_out.shape)
    return x_out


@torch_extension
def upsample2(x):
    from scipy.ndimage import zoom
    valx = x.ndim == 4
    if valx:
        x = x[0, :]
    x_out = zoom(x, (1, 2, 2))
    #x_out = x.repeat(2, axis=1).repeat(2, axis=2)
    if valx:
        x_out = np.reshape(x_out, (1,) + x_out.shape)
    return x_out


@torch_extension
def max_filter2d(x, size):
    """2d max filter with boundary reflection."""
    import scipy.ndimage
    max_filt = scipy.ndimage.filters.maximum_filter

    footprint = [1] * x.ndim
    footprint[-2] = size
    footprint[-1] = size
    res = max_filt(x, footprint)

    return res


def argmax_local2d(x, size):
    """2d argmax filter with boundary reflection."""
    return (x == max_filter2d(x, size)) * 1


@torch_extension
def max_global2d(x):
    """2d global maximum."""
    return x.max((-2, -1), keepdims=True)


@torch_extension
def circshift(x, t):
    """Circular shift with offset t."""
    x = np.roll(x, t[0], axis=-2)
    x = np.roll(x, t[1], axis=-1)
    return x


@torch_extension
def texton(x):
    from numpy.fft import rfft2, irfft2
    texton_fft = np.abs(rfft2(x))
    texton = np.real(irfft2(texton_fft))
    return texton


def cesaro(folder_path, identifier, burnin=0):
    import os
    import numpy as np
    from PIL import Image

    cesaro = 0
    cesaro_l = list()
    n = 0
    for item in sorted(os.listdir(folder_path)):
        it_nb = int('0' + ''.join(filter(str.isdigit, item)))
        if (identifier in item) and (it_nb > burnin):
            filename = folder_path + item
            new_im = np.asarray(Image.open(filename))
            cesaro = n / (n + 1) * cesaro + new_im / (n + 1)
            n += 1
            cesaro_l.append(cesaro)

    cesaro_stack = np.zeros(cesaro.shape + (1, len(cesaro_l),))
    for n in range(len(cesaro_l)):
        cesaro_stack[..., 0, n] = cesaro_l[n]

    return cesaro_stack


@torch_extension
def fourier_inv(x):
    from numpy.fft import rfft2, irfft2
    x_fft = rfft2(x)
    x_inv_fft = x_fft ** -1
    x_inv = irfft2(x_inv_fft)
    return x_inv


@torch_extension
def project_spectrum(x, x0):
    from numpy.fft import fft2, ifft2
    valx = x.ndim == 4
    if valx:
        x = x[0, :]
    if x0.ndim == 4:
        x0 = x0[0, :]
    mx = np.mean(x, (0, 1))
    mx0 = np.mean(x0, (0, 1))
    x = x - mx
    x0 = x0 - mx0
    x = x.transpose(2, 0, 1)
    x0 = x0.transpose(2, 0, 1)

    fftx0 = fft2(x0)
    fftx = fft2(x)
    prod = np.sum(np.conj(fftx0) * fftx, 0)
    phase = prod / (np.abs(prod) + np.finfo(float).eps)
    phase = phase.reshape((1,) + phase.shape)
    x_out = x_out = ifft2(phase * fftx0)
    x_out = x_out.transpose(1, 2, 0)
    x_out = np.real(x_out)
    if valx:
        x_out = np.reshape(x_out, (1,) + x_out.shape)
    return x_out


def uniform_hist(X):
    '''
    Maps data distribution onto uniform histogram

    :param X: data vector
    :return: data vector with uniform histogram
    '''

    Z = [(x, i) for i, x in enumerate(X)]
    Z.sort()
    n = len(Z)
    Rx = [0] * n
    start = 0  # starting mark
    for i in range(1, n):
        if Z[i][0] != Z[i - 1][0]:
            for j in range(start, i):
                Rx[Z[j][1]] = float(start + 1 + i) / 2.0
            start = i
    for j in range(start, n):
        Rx[Z[j][1]] = float(start + 1 + n) / 2.0
    return np.asarray(Rx) / float(len(Rx))


def histogram_matching(org_image, match_image, grey=False, n_bins=100):
    '''
    Matches histogram of each color channel of org_image with histogram of match_image
    :param org_image: image whose distribution should be remapped
    :param match_image: image whose distribution should be matched
    :param grey: True if images are greyscale
    :param n_bins: number of bins used for histogram calculation
    :return: org_image with same histogram as match_image
    '''
    import scipy.interpolate

    if grey:
        hist, bin_edges = np.histogram(
            match_image.ravel(), bins=n_bins, density=True)
        cum_values = np.zeros(bin_edges.shape)
        cum_values[1:] = np.cumsum(hist * np.diff(bin_edges))
        inv_cdf = scipy.interpolate.interp1d(
            cum_values, bin_edges, bounds_error=True)
        r = np.asarray(uniform_hist(org_image.ravel()))
        r[r > cum_values.max()] = cum_values.max()
        matched_image = inv_cdf(r).reshape(org_image.shape)
    else:
        matched_image = np.zeros_like(org_image)
        for i in range(3):
            hist, bin_edges = np.histogram(
                match_image[:, :, i].ravel(), bins=n_bins, density=True)
            cum_values = np.zeros(bin_edges.shape)
            cum_values[1:] = np.cumsum(hist * np.diff(bin_edges))
            inv_cdf = scipy.interpolate.interp1d(
                cum_values, bin_edges, bounds_error=True)
            r = np.asarray(uniform_hist(org_image[:, :, i].ravel()))
            r[r > cum_values.max()] = cum_values.max()
            matched_image[:, :, i] = inv_cdf(
                r).reshape(org_image[:, :, i].shape)

    return matched_image


@torch_extension
def affine_transfer(x, x0):
    N, M, C = x.shape
    x_out = np.empty((N, M, C))
    for i in range(C):
        a = np.std(x0[:, :, i]) / np.std(x[:, :, i])
        b = np.mean(x0[:, :, i]) - a * np.mean(x[:, :, i])
        x_out[:, :, i] = a * x[:, :, i] + b
    return x_out


@torch_extension
def sliced_wasserstein(x, x0):
    # parameters
    N = 10 ** 2
    STEP = 10 ** -2

    # 3d point clouds
    nb_pix = x.shape[0] * x.shape[1]
    x_cloud = x.reshape((nb_pix, 3))
    x0_cloud = x0.reshape((nb_pix, 3))

    # initialization
    x_out_cloud = x_cloud.astype(float)

    for k in range(N):
        u = np.random.randn(1, 3)
        x0_proj = np.dot(x0_cloud, u.T)
        x_out_proj = np.dot(x_out_cloud, u.T)
        ind_x0 = np.argsort(x0_proj[:], 0)
        ind_x_out = np.argsort(x_out_proj[:], 0)
        # stochastic gradient
        gr = (x0_proj[ind_x0] - x_out_proj[ind_x_out]).reshape((nb_pix, 1)) * u
        x_out_cloud_add = np.empty(x_out_cloud.shape)
        x_out_cloud_add_0 = x_out_cloud_add[:, 0]
        x_out_cloud_add_1 = x_out_cloud_add[:, 1]
        x_out_cloud_add_2 = x_out_cloud_add[:, 2]
        x_out_cloud_add_0[ind_x_out] = STEP * gr[:, 0].reshape(nb_pix, 1)
        x_out_cloud_add_1[ind_x_out] = STEP * gr[:, 1].reshape(nb_pix, 1)
        x_out_cloud_add_2[ind_x_out] = STEP * gr[:, 2].reshape(nb_pix, 1)
        x_out_cloud_add = np.array(
            [x_out_cloud_add_0, x_out_cloud_add_1, x_out_cloud_add_2])
        x_out_cloud += x_out_cloud_add.T

    x_out = x_out_cloud.reshape((x.shape[0], x.shape[1], 3))
    return x_out


@torch_extension
def color_transfer(x, x0, method=None):
    # good dimensions for images
    if x.ndim == 4:
        x = x[0].transpose(1, 2, 0)
    if x0.ndim == 4:
        x0 = x0[0].transpose(1, 2, 0)

    # method selection
    if method == 'histogram_matching':
        x_out = histogram_matching(x, x0)
    elif method == 'affine_transfer':
        x_out = affine_transfer(x, x0)
    elif method == 'sliced_wasserstein':
        x_out = sliced_wasserstein(x, x0)
    else:
        x_out = x
    return x_out


def correlation(buffer_x, correlation_stride):

    out = []
    for stride in correlation_stride:
        buffer_x_stride = buffer_x[::stride]
        buffer_x_stride0 = buffer_x_stride.copy()
        buffer_x_stride.append(buffer_x_stride.pop(0))
        buffer_x_stride1 = buffer_x_stride
        buffer_x_stride0.pop(-1)
        buffer_x_stride1.pop(0)
        corr = 0
        autocorr0 = 0
        autocorr1 = 0
        for k in range(len(buffer_x_stride0)):
            corr += buffer_x_stride0[k].dot(buffer_x_stride1[k])
            autocorr0 += buffer_x_stride0[k].dot(buffer_x_stride0[k])
            autocorr1 += buffer_x_stride1[k].dot(buffer_x_stride1[k])
        corr = corr / torch.sqrt(autocorr0 * autocorr1)
        out.append(corr)

    return out

    # # Load input image
    # u = np.double(plt.imread('tex/ground1013_small.png'))
    # m,n,nc=u.shape
    # (t,mv) = estimate_adsn_model(u)
    # mv = np.reshape(mv,(1,1,3))*np.ones((u.shape[0],u.shape[1],3))
    # v = adsn(t,mv)

    # # Display
    # dpi = 20
    # plt.figure(figsize=(m/float(dpi),n/float(dpi)))
    # # Original
    # plt.subplot(221)
    # plt.imshow(u)
    # plt.title('Original')
    # plt.axis('off');

    # # Gaussian Synthesis
    # plt.subplot(222)
    # plt.imshow(v)
    # plt.title('Gaussian synthesis')
    # plt.axis('off');
